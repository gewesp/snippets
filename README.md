# Snippets

Collection of random projects in different programming languages.

* [Sonification of the Riemann zeta function](mathematica/riemann-zeta)
  on the critical line. Includes a parametrization of time so that the 
  dominating frequency remains constant.
