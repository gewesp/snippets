# Introduction

The goal of this project is to:
* Make the Riemann zeta function audible on the critical line.
* Identify the dominating frequency of the oscillation of
  `zeta(1/2 + ikt)` if `t` varies over time.
* Find a function `t = f(u)` of a dimensionless variable u such that
  the dominating frequency of `zeta(1/2 + ikf(u))` remains constant
  as `u` varies.
  
The frequency increase as t increases can be heard very well
(`Play`) and seen in the `Spectrogram`.  Empirically, it looks like the
for example for k = 4000 the frequency is increasing by 200Hz for 
every doubling of t.  This suggests a logarithmic increase over time.


# Exact frequency fit

It turns out that the function `RiemannSiegelTheta` provides a perfect 
fit for the dominating frequency of `RiemannSiegelZ`.  The theta function 
is real and monotonically increasing for real arguments and
```
  ct[t_] := Cos[RiemannSiegelTheta[t]]
```
is a function with increasing frequency exactly matching the dominating
frequency of `RiemannSiegelZ`.

Let `thetainv` be the inverse function of `RiemannSiegelTheta` so that
```
  u = RiemannSiegelTheta[t] and 
  t = thetainv[u]
```
Notice that `thetainv` is well-defined for `t > 20` approximately.

If we let `u` vary,
```
  ct[u] = Cos[RiemannSiegelTheta[thetainv[u]]]
        = Cos[u]
```
Likewise, if we define
```
  Z[t_] = RiemannSiegelZ[t]
```
and then let u vary, we have
```
  Z[u] = RiemannSiegelZ[thetainv[u]]
```
which has a constant dominant frequency.


# TODO

* Document the relation between `RiemannSiegelZ` and the actual `zeta`
  function on the critical line.
